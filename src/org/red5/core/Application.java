package org.red5.core;


import org.red5.server.adapter.MultiThreadedApplicationAdapter;
import org.red5.server.api.IConnection;
import org.red5.server.api.scope.IScope;
import org.apache.commons.codec.binary.Hex;

public class Application extends MultiThreadedApplicationAdapter {
    
	RC4 crypt = new RC4("_level0".getBytes());
	
    @Override
    public boolean connect(IConnection conn, IScope scope, Object[] params){
        return true;
    }
    
    @Override
    public void disconnect(IConnection conn, IScope scope){
        super.disconnect(conn, scope);
    }
    public String $(Object[] args){
      System.out.println("CMD: " + args[0]);
	  if (args[0].equals("_I")){
		  System.out.println(Utils.H2CA(Hex.encodeHexString(crypt.encrypt("<userInitData><user Id='1' RoleFlags='0' InstructorLevel='0' MagicLevel='0' RaceLevel='0' PhoneID='1' BackgroundID='1' UsualTickets='999' MagicTickets='999' Experience='1' Level='1' PositiveEnergy='1' VisaId='1' CurrentUseCount='0' LastNewspaperId='1' IsClubPresent='false' IsMinorSecretAgent='false' IsLimited='false' HasSnFriends='false' HasInformer='false' /><account PhoneCardBalance='0' WeaponsCount='0' OxigenUnits='0' /><user_avatar><item Id='1'><row Id='1' Type='I' ItemId='3888' Layer='45' MRId='10841' /><row Id='2' Type='I' ItemId='5112' Layer='11' MRId='8122' /><row Id='3' Type='I' ItemId='6152' Layer='36' MRId='532' /><row Id='4' Type='B' ItemId='2' Color='16762375' Layer='20' MRId='56' /><row Id='5' Type='B' ItemId='30' Layer='26' MRId='67' /><row Id='6' Type='B' ItemId='31' Color='16762375' Layer='40' MRId='68' /><row Id='7' Type='B' ItemId='40' Layer='30' MRId='74' /></item></user_avatar></userInitData>".getBytes()))));
		  return Utils.H2CA(Hex.encodeHexString(crypt.encrypt("<userInitData><user Id='1' RoleFlags='0' InstructorLevel='0' MagicLevel='0' RaceLevel='0' PhoneID='1' BackgroundID='1' UsualTickets='999' MagicTickets='999' Experience='1' Level='1' PositiveEnergy='1' VisaId='1' CurrentUseCount='0' LastNewspaperId='1' IsClubPresent='1' IsMinorSecretAgent='false' IsLimited='false' HasSnFriends='false' HasInformer='false' /><account PhoneCardBalance='0' WeaponsCount='0' OxigenUnits='0' /><user_avatar><item Id='1'><row Id='1' Type='I' ItemId='3888' Layer='45' MRId='10841' /><row Id='2' Type='I' ItemId='5112' Layer='11' MRId='8122' /><row Id='3' Type='I' ItemId='6152' Layer='36' MRId='532' /><row Id='4' Type='B' ItemId='2' Color='16762375' Layer='20' MRId='56' /><row Id='5' Type='B' ItemId='30' Layer='26' MRId='67' /><row Id='6' Type='B' ItemId='31' Color='16762375' Layer='40' MRId='68' /><row Id='7' Type='B' ItemId='40' Layer='30' MRId='74' /></item></user_avatar></userInitData>".getBytes())));
	  } else if (args[0].equals("_CM")){
		  return "<clubmap/>";
	  } else if (args[0].equals("_D")) {
		  // тут данные пользователя
		  // нужно отправить с серва на клиент запрос на выполенение функции nc._D 
		  }  else if (args[0].equals("_LG")){
			return "<location ID='28' IsHome='0' IsClub='0' IsLocked='0' IsVisaAccessPermitted='1' MediaResourceID='30644' x='5500' y='3800' Name='test'>   <options>     <Constraints>       <item ID='0' Type='Q' QuestId='390' StepId='23' StateCode='1' Equally='1' Value='2' RoomState='0' />    </Constraints>  </options>  <object ID='28002' AObjectTypeId='2' AObjectId='61' AObjectRefId='25' MediaResourceID='30648' x='6340' y='1970' />   <object ID='28003' AObjectTypeId='2' AObjectId='70' AObjectRefId='19' MediaResourceID='30645' x='6740' y='2270' />   <object ID='28004' AObjectTypeId='2' AObjectId='71' AObjectRefId='30' MediaResourceID='30647' x='1350' y='1530' />   <object ID='28010' AObjectTypeId='2' AObjectId='532' AObjectRefId='54' MediaResourceID='3587' x='1300' y='3300'>     <options>       <override AObjectTypeId='18' />     <conditions/>     </options>  </object>   <object ID='28733' AObjectTypeId='2' AObjectId='733' AObjectRefId='6' MediaResourceID='30646' x='4900' y='1200' /> </location>"; // вот тут XML локации
		  } else if (args[0].equals("_DA")){
		 return "<conditions><User IsMember=\"false\" IsSecretAgent=\"false\" IsPresent=\"true\" IsActive=\"true\" Units=\"10\" /></conditions>";
	  }
      return "";
	  }	  

 
    public String _LS(Object[] parameters){
    	System.out.println("lol!");
    	return "<first><location><object><options><Constraints><item Code='1' Equally='1' Value='2' RoomState='0' /><item ID='1' Type='Q' QuestId='429' StepId='28' StateCode='1' Equally='1' Value='1' RoomState='0' /></Constraints><behaviour IsPositionHeld='1' /></options></object><object ID='6007' AObjectTypeId='21' AObjectId='10' MediaResourceID='4317' x='60.0' y='268.1'><options><behaviour IsPositionHeld='1' /><general SObjectTypeId='1' PopupId='4304' /></options></object><object ID='6013' AObjectTypeId='21' AObjectId='3' MediaResourceID='4318' x='543.0' y='220.0'><options><behaviour IsPositionHeld='1' /><general SObjectTypeId='1' PopupId='4984' /></options></object><object ID='6330' AObjectTypeId='3' AObjectId='115' AObjectRefId='41' MediaResourceID='1567' x='547.0' y='160.0'><options><behaviour IsPositionHeld='1' /></options></object></location></first>";
    }
    public String _SCD(Object[] parameters){
    	return "";
    }
}
