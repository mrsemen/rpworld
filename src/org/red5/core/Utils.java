package org.red5.core;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class Utils {
	public static String H2CA(String h){
		String pisos = H2S(h);
		byte[] ascii = pisos.getBytes(StandardCharsets.US_ASCII);
		String asciiString = Arrays.toString(ascii);
		return asciiString;
	}

	public static String H2S(String hex){
	  StringBuilder sb = new StringBuilder();
	  StringBuilder temp = new StringBuilder();
	  
	  for( int i=0; i<hex.length()-1; i+=2 ){
	      String output = hex.substring(i, (i + 2));
	      int decimal = Integer.parseInt(output, 16);
	      sb.append((char)decimal);
	      temp.append(decimal);
	  }
  
	  return sb.toString();
	}
	
}
